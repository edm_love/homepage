import Vue from 'vue';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/analytics";

import { firestorePlugin } from 'vuefire'

Vue.use(firestorePlugin)

import App from './App.vue';
import { firebaseConfig } from './lib/firebase';

import router from './router';
import store from './lib/store';

Vue.config.productionTip = false;
firebase.initializeApp(firebaseConfig);

const authChange = firebase.auth().onAuthStateChanged((user) => {
  store.dispatch('fetchUser', user);
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app');

  authChange();

});
